package sevice;

import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;

public class etudiantDAO extends JDBC{
	
	public String ajouter(etudiant c) throws ClassNotFoundException,SQLException {
		try{
			prepareStat = this.jdbc().prepareStatement("insert into java.etudiant values (default, ?, ?, ?,?)");
			prepareStat.setString(1, c.getNom());
			prepareStat.setString(2, c.getPrenom());
			prepareStat.setString(3, c.getSex());
			prepareStat.setString(4, c.getDaten());
			//prepareStat.setdate(4, new java.sql.date(2009, 12, 11));
			prepareStat.executeUpdate();
			return "ok";
		} catch (SQLException e) {
			return e.getMessage();
		}
	}
	
	public List<etudiant> listes() throws ClassNotFoundException,SQLException {
			statment = this.jdbc().createStatement();
			List<etudiant> liste = new ArrayList<etudiant>();
			etudiant e = new etudiant();
			result = statment.executeQuery("select * from etudiant");
			while(result.next()){
				e.setId(Integer.valueOf(result.getString("id_etudiant")));
				e.setNom(result.getString("nom"));
				e.setPrenom(result.getString("prenom"));
				e.setSex(result.getString("sexe"));
				e.setDaten(result.getString("date_niassance"));
				liste.add(e);
				e = new etudiant();
				}
			return liste;
		
	}
	
	
	public String update(etudiant c) throws ClassNotFoundException,SQLException {
		try{
			String sql ="UPDATE etudiant SET nom=?,prenom=?,sexe=?,date_niassance=? WHERE id_etudiant=?";
			prepareStat = this.jdbc().prepareStatement(sql);
			prepareStat.setString(1, c.getNom());
			prepareStat.setString(2, c.getPrenom());
			prepareStat.setString(3, c.getSex());
			prepareStat.setString(4, c.getDaten());
			prepareStat.setInt(5, c.getId());
			
			int rowsDeleted = prepareStat.executeUpdate();
			
			prepareStat.executeUpdate();
			if (rowsDeleted > 0){
				System.out.println("l'etudiant a ete modifier avec succes ");
			}
			return "ok";
			
		} catch (SQLException ex) {
			return ex.getMessage();
			
		}
	
		}
	
	public etudiant findByID(int id_etudiant) throws ClassNotFoundException,SQLException {
		statment = this.jdbc().createStatement();
		etudiant e = new etudiant();
		prepareStat =this.jdbc().prepareStatement("select * from etudiant where id_etudiant=?");
		prepareStat.setInt(1,id_etudiant);
		
		result =prepareStat.executeQuery();
		
		if(result.next()){
			e.setId(Integer.valueOf(result.getString("id_etudiant").replace("","")));
			e.setNom(result.getString("nom"));
			e.setPrenom(result.getString("prenom"));
			e.setSex(result.getString("sexe"));
			e.setDaten(result.getString("date_niassance"));
			
		
			}
		return e;
	
}
	
	public String supprimer(int id) throws ClassNotFoundException,SQLException {
		try{
			prepareStat = this.jdbc().prepareStatement("DELETE FROM `etudiant` WHERE `etudiant`.`id_etudiant` = ?");
			prepareStat.setInt(1, id);
			prepareStat.executeUpdate();
			return "ok";
		} catch (SQLException e) {
			return e.getMessage();
		}
	}
}
