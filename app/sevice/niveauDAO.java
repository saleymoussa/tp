package sevice;

import java.util.List;

import java.util.ArrayList;
import java.sql.SQLException;
public class niveauDAO extends JDBC{
	public String ajouter(niveau n) throws ClassNotFoundException,SQLException {
		try{
			prepareStat = this.jdbc().prepareStatement("insert into java.niveau values (default, ?, ?)");
			prepareStat.setString(1, n.getNiveau());
			prepareStat.setInt(2, n.getMontant());
			
			//prepareStat.setdate(4, new java.sql.date(2009, 12, 11));
			prepareStat.executeUpdate();
			return "ok";
		} catch (SQLException e) {
			return e.getMessage(); 
		}
	}
		public List<niveau> listes() throws ClassNotFoundException,SQLException {
			statment = this.jdbc().createStatement();
			List<niveau> liste = new ArrayList<niveau>();
			niveau e = new niveau();
			result = statment.executeQuery("select * from niveau");
			while(result.next()){
				e.setId(Integer.valueOf(result.getString("id_niveau")));
				e.setNiveau(result.getString("niveau"));
				e.setMontant(result.getInt("montant"));
				
				
				liste.add(e);
				e = new niveau();
				}
			return liste;
		
	}
		public String update(niveau c) throws ClassNotFoundException,SQLException {
			try{
				String sql ="UPDATE etudiant SET niveau=?,montant=?, WHERE id_niveau=?";
				prepareStat = this.jdbc().prepareStatement(sql);
				prepareStat.setString(1, c.getNiveau());
				prepareStat.setInt(2, c.getMontant());
				prepareStat.setInt(5, c.getId());
				
				int rowsDeleted = prepareStat.executeUpdate();
				
				prepareStat.executeUpdate();
				if (rowsDeleted > 0){
					System.out.println("le niveau a ete modifier avec succes ");
				}
				return "ok";
				
			} catch (SQLException ex) {
				return ex.getMessage();
				
			}
	}
			public niveau findByID(int id_niveau) throws ClassNotFoundException,SQLException {
				statment = this.jdbc().createStatement();
				niveau e = new niveau();
				prepareStat =this.jdbc().prepareStatement("select * from etudiant where id_etudiant=?");
				prepareStat.setInt(1,id_niveau);
				
				result =prepareStat.executeQuery();
				
				if(result.next()){
					e.setId(Integer.valueOf(result.getString("id_niveau").replace("","")));
					e.setNiveau(result.getString("niveau"));
					e.setMontant(result.getInt("montant"));
					
				
					}
				return e;
			
		}
			
			public String supprimer(int id) throws ClassNotFoundException,SQLException {
				try{
					prepareStat = this.jdbc().prepareStatement("DELETE FROM `niveau` WHERE `niveau`.`id_niveau` = ?");
					prepareStat.setInt(1, id);
					prepareStat.executeUpdate();
					return "ok";
				} catch (SQLException e) {
					return e.getMessage();
				}
			}
		}

