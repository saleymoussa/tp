package controllers;

import java.sql.SQLException;
import javax.inject.Inject;
import sevice.etudiant;
import sevice.etudiantDAO;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.Result;

public class etudiantCtrl extends Controller {
	FormFactory dataForm;
	etudiantDAO etudiantDAO;
	
	@Inject
	public etudiantCtrl(FormFactory dataForm, etudiantDAO EtudiantDAO) {
		super();
		this.dataForm = dataForm;
		this.etudiantDAO = EtudiantDAO;
	}
	
public Result afficherFormulaire( Request request) throws ClassNotFoundException, SQLException {
		
		return ok(views.html.ajouter.render(request));
		
	} 
	
	public Result ajouterEtudiant(Request request) throws ClassNotFoundException, SQLException {
		  Form<etudiant> form = dataForm.form(etudiant.class).bindFromRequest(request);
		  etudiant Etudiant = form.get();
	      
	      if(etudiantDAO.ajouter(Etudiant).equals("ok"))
	    	  System.out.println("l'ajout effectuer avec success");
	      else
	    	  System.out.println("l'ajout a echoue : " + etudiantDAO.ajouter(Etudiant)); 
	      return ok(views.html.Afficheretudiant.render(etudiantDAO.listes()));
		
	}
	
	
	
public Result modifview( Request request, int id_etudiant) throws ClassNotFoundException, SQLException {
		
		return ok(views.html.modifieretudiant.render(etudiantDAO.findByID(id_etudiant),request));
		
	} 
	
	public Result updateE(Request request) throws ClassNotFoundException, SQLException {
		  Form<etudiant> form = dataForm.form(etudiant.class).bindFromRequest(request);
		  etudiant e = form.get();
	      
	      if(etudiantDAO.update(e).equals("ok"))
	    	  System.out.println("Modification effectuer avec success");
	      else
	    	  System.out.println("Modification a echoue : " + etudiantDAO.update(e)); 
	      return ok(views.html.Afficheretudiant.render(etudiantDAO.listes()));
		
	}
	
	
public Result afficherlistes( Request request) throws ClassNotFoundException, SQLException {
		
	return ok(views.html.Afficheretudiant.render(etudiantDAO.listes()));
		
	} 
public Result delete( Request request,int id) throws ClassNotFoundException, SQLException {
	etudiantDAO.supprimer(id);
	return ok(views.html.Afficheretudiant.render(etudiantDAO.listes()));
		
	} 
	

	

}
