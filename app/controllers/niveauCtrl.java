package controllers;

import java.sql.SQLException;

import javax.inject.Inject;
import sevice.niveau;
import sevice.niveauDAO;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.Result;

public class niveauCtrl extends Controller {
	
	
	FormFactory dataForm;
	niveauDAO niveauDAO;
	
	@Inject
	public niveauCtrl(FormFactory dataForm, niveauDAO NiveauDAO) {
		super();
		this.dataForm = dataForm;
		this.niveauDAO = NiveauDAO;
	}
	
public Result afficherFormulaire( Request request) throws ClassNotFoundException, SQLException {
		
		return ok(views.html.ajouterniveau.render(request));
		
	} 
	
	public Result ajouterNiveau(Request request) throws ClassNotFoundException, SQLException {
		  Form<niveau> form = dataForm.form(niveau.class).bindFromRequest(request);
		  niveau niveau = form.get();
	      
	      if(niveauDAO.ajouter(niveau).equals("ok"))
	    	  System.out.println("l'ajout effectuer avec success");
	      else
	    	  System.out.println("l'ajout a echoue : " + niveauDAO.ajouter(niveau)); 
	      return ok(views.html.afficherniveau.render(niveauDAO.listes()));
		
	}
	
	
	
public Result modifview( Request request, int id_niveau) throws ClassNotFoundException, SQLException {
		
		return ok(views.html.modifierniveau.render(niveauDAO.findByID(id_niveau),request));
		
	} 
	
	public Result updateE(Request request) throws ClassNotFoundException, SQLException {
		  Form<niveau> form = dataForm.form(niveau.class).bindFromRequest(request);
		  niveau e = form.get();
	      
	      if(niveauDAO.update(e).equals("ok"))
	    	  System.out.println("Modification effectuer avec success");
	      else
	    	  System.out.println("Modification a echoue : " + niveauDAO.update(e)); 
	      return ok(views.html.afficherniveau.render(niveauDAO.listes()));
		
	}
	
	
public Result afficherlistes( Request request) throws ClassNotFoundException, SQLException {
		
	return ok(views.html.afficherniveau.render(niveauDAO.listes()));
		
	} 
public Result delete( Request request,int id) throws ClassNotFoundException, SQLException {
	niveauDAO.supprimer(id);
	return ok(views.html.afficherniveau.render(niveauDAO.listes()));
		
	} 
	

	


}
